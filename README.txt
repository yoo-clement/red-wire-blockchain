Pour lancer la blockchain :

A la racine du dossier red-wire-blockchain, lancer la commande python app.py

Le script send-money.py permet d'ajouter une transaction à la blockchain : python send-money.py <montant> <sender> <recipient>